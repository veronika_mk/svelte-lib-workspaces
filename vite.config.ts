import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';
import { svelte } from '@sveltejs/vite-plugin-svelte';
import * as path from 'path';
// import * as path from 'path';

export default defineConfig({
	// plugins: [sveltekit()],
	plugins: [svelte()],
	build: {
		outDir: 'dist',
		emptyOutDir: true,
		// rollupOptions: {
		// 	// into your library
		// 	external: ['svelte'],
		// 	output: {
		// 		// Provide global variables to use in the UMD build
		// 		// for externalized deps
		// 		globals: {
		// 			svelte: 'Svelte'
		// 		}
		// 	}
		// }
	}
});

// // ** @type {import('vite').UserConfig}
// const config = {
// 	plugins: [sveltekit()],
// 	test: {
// 		include: ['src/**/*.{test,spec}.{js,ts}'],
// 	},
// 	build: {
// 		chunkSizeWarningLimit: 1600,
// 	},
// 	resolve: {
// 		alias: {
// 			'@glance-design': path.resolve(process.cwd(), 'src/lib/components/index.js'),
// 		}
// 	}
// };
//
// // export default defineConfig(config);
// export default config;