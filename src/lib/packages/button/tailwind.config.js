import { Config } from "@vrn-design/tailwind-config";

const config = {
  presets: [Config],
  content: [
    './src/**/**.{svelte,js,ts}',
    // '../../node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'
  ],
};

export default config;