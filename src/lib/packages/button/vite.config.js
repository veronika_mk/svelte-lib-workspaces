import {defineConfig} from 'vite';
import {svelte} from '@sveltejs/vite-plugin-svelte';

export default defineConfig({
  plugins: [svelte()],
  build: {
    lib: {
      entry: 'src/index.js',
      name: 'Button',
    },
    outDir: 'dist2',
    emptyOutDir: true,
  }
});