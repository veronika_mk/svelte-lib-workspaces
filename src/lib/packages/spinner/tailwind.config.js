import { Config } from "@vrn-design/tailwind-config";

const config = {
  presets: [Config],
  content: [
    './src/**/*.{svelte,js,ts}',
  ],
};

export default config;