import tw from 'tailwindcss/defaultConfig';

/**
 * @name spacingExtended
 * @description Adds additional spacing tokens
 * @type {{}|*}
 */
export const spacingExtended = {
  '0.75': '0.188rem', // 3px
  '4.5': '1.125rem', // 18px
  '5.5': '1.375rem', // 22px
  '10.5': '2.625rem', // 42px
  '11.5': '2.875rem', // 46px
  '13': '3.375rem', // 54px
  '18': '4.25rem', // 68px
};

/**
 * @name spacing
 * @description Combines tailwind spacing tokens and extended spacing tokens
 * @type {{}|*}
 */
export const spacing = {
  ...tw?.theme?.spacing,
  ...spacingExtended,
};