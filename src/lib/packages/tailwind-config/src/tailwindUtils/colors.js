import tw from 'tailwindcss/defaultConfig';
// @ts-expect-error type
import * as tailwindTokens from '@glance-ui/supernova-tokens';

const tailwindColors = tailwindTokens.tailwindVariables.colors;

/**
 * @name customColorTokens
 * @description Adds additional custom color tokens only for old components that are not using tokens
 * TODO: Can be removed after design update is complete and all components are using tokens
 * @type {{}|*}
 */
const customColorTokens = {
  'neutral-0': '#FFFFFF',
  'slate-75': '#E4E8F0',
  'viewer-slate-100': '#F1F5F9',
  'slate-175': '#B2B5BB',
  'slate-900': '#14161C',
  'indigo-600': '#0C46AB',
  'blue-600': '#019ADA',
  'yellow-500': '#F87F01',
  'green-600': '#03C99B',
};

/**
 * @name colorTokens
 * @description Combines tailwind base colors, custom colors and supernova tokens colors
 * @type {{}|*}
 */
export const colorTokens = {
  // tailwind base colors
  ...tw?.theme?.colors,
  // custom colors
  ...customColorTokens,
  // supernova tokens colors
  ...tailwindColors,
  'tertiary-border-disabled': '#E2E8F0'
};