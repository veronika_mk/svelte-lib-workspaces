import resolveConfig from 'tailwindcss/resolveConfig';
import plugin from 'tailwindcss/plugin';
import { typography } from "./tailwindUtils/typography.js";
import { InterFontFamily, FiraCodeFontFamily } from "./tailwindUtils/font.js";
import { spacing } from "./tailwindUtils/spacing.js";
import { colorTokens } from "./tailwindUtils/colors.js";


const tailwindConfig = {
  content: [
    '../../node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}',
    './node_modules/@vrn-design/**/**/*.{js,svelte,ts}',
    // path.resolve(path.dirname(require.resolve('@repo/ui')), '**/*.{html,js,svelte,ts}'),
  ],
  theme: {
    extend: {
      colors: {
        ...colorTokens,
      },
      spacing,
      lineHeight: {
        '4.5': '1.125rem', // 18px
        '5.5': '1.375rem', // 22px
      },
      outlineWidth: {
        3: '3px',
      },
      outlineOffset: {
        3: '3px',
      },
      fontFamily: {
        sans: ['Inter', 'sans-serif', 'system-ui', 'ui-sans-serif'],
      },
      /* @ts-expect-error theme */
      boxShadow: (theme) => ({
        'elevation-light-shadow-none': `0 0 0 1px ${theme("colors.neutral-surface.inverse-dim / 0.1")}`,
        'elevation-light-shadow': `0px 1px 3px 0 ${theme("colors.neutral-surface.inverse-dim / 0.1")}, 0px 1px 2px -1px ${theme("colors.neutral-surface.inverse-dim / 0.1")}`,
        "elevation-light-shadow-md": `0px 4px 6px 1px ${theme("colors.neutral-surface.inverse-dim / 0.1")}, 0px 2px 4px -2px ${theme("colors.neutral-surface.inverse-dim / 0.1")}`,
        "elevation-light-shadow-lg": `0px 10px 15px -3px ${theme("colors.neutral-surface.inverse-dim / 0.1")}, 0px 4px 10px -4px ${theme("colors.neutral-surface.inverse-dim / 0.1")}`,
        "elevation-light-shadow-xl": `0px 20px 25px -5px ${theme("colors.neutral-surface.inverse-dim / 0.1")}, 0px 8px 19px -6px ${theme("colors.neutral-surface.inverse-dim / 0.1")}`,
        "elevation-light-shadow-2xl": `0px 25px 50px -12px ${theme("colors.neutral-surface.inverse-dim / 0.25")}`,
      }),
    },
  },
  plugins: [
    plugin(function ({ addBase }) {
      InterFontFamily.forEach((fontProperties) => {
        addBase(fontProperties);
      });

      FiraCodeFontFamily.forEach((fontProperties) => {
        addBase(fontProperties);
      });
    }),
    plugin(function ({ addUtilities }) {
      /* @ts-expect-error theme */
      addUtilities(typography)
    }),
  ],
};

export const { theme } = resolveConfig(tailwindConfig);

export default tailwindConfig;