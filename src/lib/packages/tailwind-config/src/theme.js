import Config, { theme } from './tailwind.config.js';

export { theme, Config };

export default Config;