import { writable } from 'svelte/store';
import { setContext, getContext } from 'svelte';
import type { AccordionStoreOptions, StoreContext } from './types';
import { ACTIONS } from './constants';

// Set the global context for the accordion
export function setStore({
	autocollapse,
	disabled,
	action = ACTIONS.click,
	state,
	size,
	rtl,
	variant,
	skeleton
}: Partial<AccordionStoreOptions>): StoreContext {
	const store = writable({
		keys: {},
		disabledKeys: {},
		disabled,
		action,
		autocollapse,
		state,
		size,
		rtl,
		variant,
		skeleton
	});

	return setContext<StoreContext>('Accordion-store', {
		...store,
		setItemStateById: (id: string, state: boolean, autocollapse?: boolean) => {
			if (autocollapse) {
				store.update((s) => Object.assign(s, { keys: { [id]: state } }));
			} else {
				store.update((s) => Object.assign(s, { keys: { ...s.keys, [id]: state } }));
			}
		},
		setDisabledById: (id, disabledProp) => {
			store.update((s) =>
				Object.assign(s, {
					disabledKeys: { ...s.disabledKeys, [id]: Boolean(disabledProp || disabled) },
					keys: { ...s.keys, [id]: disabledProp || disabled ? false : s.keys[id] }
				})
			);
		},
		setDisabled: (disabledProp) => {
			store.update((s) =>
				Object.assign(s, {
					disabled: Boolean(disabledProp),
					disabledKeys: Object.keys(s.keys).reduce((acc, key) => {
						acc[key] = Boolean(disabledProp);
						return acc;
					}, {}),
					keys: Object.keys(s.keys).reduce((acc, key) => {
						acc[key] = disabledProp ? false : s.keys[key];
						return acc;
					}, {})
				})
			);
		},
		toggleExpanded: (expanded) => {
			store.update((s) =>
				Object.assign(s, {
					keys: Object.keys(s.keys).reduce((acc, key) => {
						acc[key] = expanded;
						return acc;
					}, {})
				})
			);
		}
	});
}

// Get the global context for the accordion
export function getStore() {
	return getContext<StoreContext>('Accordion-store');
}
