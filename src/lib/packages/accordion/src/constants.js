export const ACTIONS = Object.freeze({
  click: 'click',
  hover: 'hover',
});

export const STATES = Object.freeze({
  expanded: 'expanded',
  collapsed: 'collapsed',
});

export const ACCORDION_SIZES = Object.freeze({
  'x-small': 'x-small',
  small: 'small',
  medium: 'medium',
  large: 'large',
});

export const ACCORDION_VARIANTS = Object.freeze({
  border: 'border',
  fill: 'fill',
});


export const ACCORDION_DEFAULT_PROPS = {
  action: ACTIONS.click,
  autocollapse: false,
  disabled: false,
  state: STATES.collapsed,
  size: ACCORDION_SIZES.medium,
  rtl: false,
  variant: ACCORDION_VARIANTS.border,
  skeleton: false,
}

export const ARROW_SIZES = {
  [ACCORDION_SIZES['x-small']]: '4',
  [ACCORDION_SIZES.small]: '4.5',
  [ACCORDION_SIZES.medium]: '5',
  [ACCORDION_SIZES.large]: '5.5',
};
