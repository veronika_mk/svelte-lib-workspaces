import type { Writable } from "svelte/store";

// Global context
export type AccordionStoreOptions = {
  autocollapse: boolean,
  action: string,
  state: string,
  disabled: boolean,
  size: string,
  rtl: boolean,
  variant: string,
  skeleton: boolean,
  keys: Record<string, boolean>,
  disabledKeys: Record<string, boolean>,
};

export type Item = { id: string, expanded: boolean };
export type StoreContext = Writable<{
  keys: Record<string, boolean>,
  disabledKeys: Record<string, boolean>,
  autocollapse?: boolean,
  action?: string,
  state?: string,
  disabled?: boolean,
  skeleton?: boolean,
}> & {
  setItemStateById: (id: string, state: boolean, autocollapse?: boolean) => void;
  setDisabledById: (id: string, disabled: boolean) => void,
  setDisabled: (disabled: boolean) => void,
  toggleExpanded: (expanded: boolean) => void,
};

// Local context
export type Label = string | null;
