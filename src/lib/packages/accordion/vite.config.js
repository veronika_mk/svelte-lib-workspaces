import {defineConfig} from 'vite';
import {svelte} from '@sveltejs/vite-plugin-svelte';

export default defineConfig({
  plugins: [svelte()],
  test: {
    environment: 'jsdom',
    globals: true,
    alias: [{ find: /^svelte$/, replacement: 'svelte/internal'}]
  },
  build: {
    lib: {
      entry: 'src/index.js',
      name: 'Button',
    },
    outDir: 'dist2',
    emptyOutDir: true,
  }
});