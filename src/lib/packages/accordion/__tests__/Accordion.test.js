import { cleanup, render, screen, waitFor } from "@testing-library/svelte";
import userEvent from "@testing-library/user-event";
import { Accordion } from "../src";
import AccordionSlot from "./AccordionSlot.mock.svelte";
import { STATES, ACTIONS } from "../src/constants";
import { describe, it, afterEach, expect } from 'vitest';
import '@testing-library/jest-dom';

const setup = (Component, props = {}) => {
  return {
    user: userEvent.setup(),
    ...render(Component, props),
  };
};
const getAccordion = () => screen.getByTestId("Accordion");

describe("<Accordion />", () => {
  afterEach(cleanup);

  it("Should not explode without props", async () => {
    setup(Accordion);

    expect(getAccordion()).toBeInTheDocument();
  });

  describe("<Skeleton />", () => {
    it("Should render skeleton", async () => {
      setup(AccordionSlot, {
        props: {
          skeleton: true,
          Component: Accordion,
        },
      });

      expect(screen.getByTestId("Accordion-skeleton")).toBeInTheDocument();
      expect(screen.queryByTestId("Accordion")).not.toBeInTheDocument();
      expect(screen.getAllByTestId('Accordion-title-skeleton')).toHaveLength(2);
    });

    it("Should render expanded skeleton options", async () => {
      setup(AccordionSlot, {
        props: {
          skeleton: true,
          state: "expanded",
          Component: Accordion,
        },
      });

      expect(screen.getAllByTestId('Accordion-title-skeleton')).toHaveLength(2);
      expect(screen.getAllByTestId('Accordion-content-skeleton')).toHaveLength(2);
    });

    it("Should render collapsed skeleton options", async () => {
      setup(AccordionSlot, {
        props: {
          skeleton: true,
          state: "collapsed",
          Component: Accordion,
        },
      });

      expect(screen.getAllByTestId('Accordion-title-skeleton')).toHaveLength(2);
      expect(screen.queryAllByTestId('Accordion-content-skeleton')).toHaveLength(0);
    });

    it("Should respect rtl skeleton prop", async () => {
      setup(AccordionSlot, {
        props: { rtl: true, skeleton: true, state: "expanded", Component: Accordion },
      });

      expect(screen.getAllByTestId('Accordion-title-skeleton')[0]).toHaveClass('accordion-title-rtl');
      expect(screen.getAllByTestId('Accordion-content-skeleton')[0]).toHaveClass('accordion-content-rtl');
    });
  });

  it("Should render slots", async () => {
    setup(AccordionSlot, {
      props: { action: ACTIONS.click, Component: Accordion },
    });

    expect(screen.getByText("Option 1")).toBeInTheDocument();
    expect(screen.getByText("Option 2")).toBeInTheDocument();
  });

  it("Should fallback to collapsed if state prop is unknown", async () => {
    setup(AccordionSlot, {
      props: {
        action: ACTIONS.click,
        state: "anyString",
        Component: Accordion,
      },
    });

    expect(screen.queryByText("Option 1 Description")).not.toBeInTheDocument();
    expect(screen.queryByText("Option 2 Description")).not.toBeInTheDocument();
  });

  it("Should open items slots description", async () => {
    setup(AccordionSlot, {
      props: {
        action: ACTIONS.click,
        state: STATES.expanded,
        Component: Accordion,
      },
    });

    expect(screen.getByText("Option 1 Description")).toBeInTheDocument();
    expect(screen.getByText("Option 2 Description")).toBeInTheDocument();
  });

  it("Should close items slots description", async () => {
    setup(AccordionSlot, {
      props: {
        action: ACTIONS.click,
        state: STATES.collapsed,
        Component: Accordion,
      },
    });

    expect(screen.queryByText("Option 1 Description")).not.toBeInTheDocument();
    expect(screen.queryByText("Option 2 Description")).not.toBeInTheDocument();
  });

  it("Should respect accordion items disabled prop", async () => {
    setup(AccordionSlot, {
      props: {
        action: ACTIONS.click,
        disabled: true,
        autocollapse: true,
        Component: Accordion,
      },
    });
    const options = screen.getAllByRole("button");

    expect(screen.queryByText("Option 1 Description")).not.toBeInTheDocument();
    expect(screen.queryByText("Option 2 Description")).not.toBeInTheDocument();

    await waitFor(() => {
      options.forEach((option) => {
        expect(option).toBeDisabled();
      });
    });
  });

  it("Should respect rtl prop", async () => {
    setup(AccordionSlot, {
      props: {
        action: ACTIONS.click,
        rtl: true,
        state: STATES.expanded,
        Component: Accordion,
      },
    });
    expect(screen.getAllByTestId('accordion-button')[0]).toHaveClass('accordion-title-rtl');
    expect(screen.getAllByTestId('accordion-content')[0]).toHaveClass('accordion-content-rtl');
  });

  describe("Size", () => {
    it("Should respect x-small size prop", async () => {
      setup(AccordionSlot, {
        props: {
          action: ACTIONS.click,
          size: "x-small",
          state: STATES.expanded,
          Component: Accordion,
        },
      });
      expect(screen.getAllByTestId('accordion-button')[0]).toHaveClass('accordion-title-size--x-small');
      expect(screen.getAllByTestId('accordion-content')[0]).toHaveClass('accordion-content-size--x-small');
    });


    it("Should respect small size prop", async () => {
      setup(AccordionSlot, {
        props: {
          action: ACTIONS.click,
          size: "small",
          state: STATES.expanded,
          Component: Accordion,
        },
      });
      expect(screen.getAllByTestId('accordion-button')[0]).toHaveClass('accordion-title-size--small');
      expect(screen.getAllByTestId('accordion-content')[0]).toHaveClass('accordion-content-size--small');
    });

    it("Should respect medium size prop", async () => {
      setup(AccordionSlot, {
        props: {
          action: ACTIONS.click,
          size: "medium",
          state: STATES.expanded,
          Component: Accordion,
        },
      });
      expect(screen.getAllByTestId('accordion-button')[0]).toHaveClass('accordion-title-size--medium');
      expect(screen.getAllByTestId('accordion-content')[0]).toHaveClass('accordion-content-size--medium');
    });

    it("Should respect large size prop", async () => {
      setup(AccordionSlot, {
        props: {
          action: ACTIONS.click,
          size: "large",
          state: STATES.expanded,
          Component: Accordion,
        },
      });
      expect(screen.getAllByTestId('accordion-button')[0]).toHaveClass('accordion-title-size--large');
      expect(screen.getAllByTestId('accordion-content')[0]).toHaveClass('accordion-content-size--large');
    });
  });

  describe('Variant', () => {
    it('Should respect variant - border', async () => {
      setup(AccordionSlot, {
        props: {
          action: ACTIONS.click,
          variant: 'border',
          state: STATES.expanded,
          Component: Accordion,
        },
      });
      expect(screen.getAllByTestId('Accordion-item')[0]).not.toHaveClass('accordion-variant--fill');
    });

    it('Should respect variant - fill', async () => {
      setup(AccordionSlot, {
        props: {
          action: ACTIONS.click,
          variant: 'fill',
          state: STATES.expanded,
          Component: Accordion,
        },
      });
      expect(screen.getAllByTestId('Accordion-item')[0]).toHaveClass('accordion-variant--fill');
    });
  });

  it("Should respect action: click prop", async () => {
    setup(AccordionSlot, {
      props: { action: ACTIONS.click, Component: Accordion },
    });

    const button = screen.getAllByRole("button")[0];
    await userEvent.click(button);

    expect(screen.getByText("Option 1 Description")).toBeInTheDocument();
  });

  it("Should respect action: hover prop", async () => {
    const { user } = setup(AccordionSlot, {
      props: { action: ACTIONS.hover, Component: Accordion },
    });

    const button = screen.getAllByRole("button")[0];
    await user.pointer({ target: button });

    expect(screen.getByText("Option 1 Description")).toBeInTheDocument();
  });
});

