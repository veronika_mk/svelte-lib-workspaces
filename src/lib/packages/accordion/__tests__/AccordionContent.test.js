import { cleanup, render, screen } from "@testing-library/svelte";
import AccordionItemSlot from "./AccordionItemSlot.mock.svelte";
import AccordionContent from "../src/AccordionContent.svelte";
import { describe, it, afterEach, expect } from 'vitest';
import '@testing-library/jest-dom';

const setup = (Component, props = {}) => {
  return {
    ...render(Component, props),
  };
};

describe("<AccordionContent />", () => {
  afterEach(cleanup);

  it('should respect expanded false prop', () => {

    setup(AccordionItemSlot, { props: { root: { expanded: false, id: '12345' }, content: 'Text label content' } })

    expect(screen.queryByText('Text label content')).not.toBeInTheDocument();
  });

  it("Should respect slot rather than content prop", async () => {
    setup(AccordionItemSlot, { props: { root: { expanded: true }, contentSlot: 'Slot content' } })

    expect(screen.getByText('Slot content')).toBeInTheDocument();
  });

  it("Should has role=region", async () => {
    setup(AccordionItemSlot, { props: { root: { expanded: true, id: '12345' }, label: 'Text label content' } })

    expect(screen.getByTestId('accordion-content')).toHaveAttribute('role', 'region');
  });

  it("Should has aria-labelledby", async () => {
    const { component } = setup(AccordionContent)
    await component.$set({ expanded: true, key: '12345' });

    expect(screen.getByTestId('accordion-content')).toHaveAttribute('aria-labelledby', `accordion-control-12345`);
  });

  describe('Skeleton', () => {
    it('Should render skeleton when loading', async () => {
      const { component } = setup(AccordionContent)
      await component.$set({ skeleton: true, expanded: true, key: '12345' });

      expect(screen.getByTestId('Accordion-content-skeleton')).toBeInTheDocument();
    });

    it('Should not render skeleton when not loading', async () => {
      const { component } = setup(AccordionContent)
      await component.$set({ skeleton: false, expanded: true, key: '12345' });

      expect(screen.queryByTestId('Accordion-content-skeleton')).not.toBeInTheDocument();
    });
  });

  describe('RTL', () => {
    it('Should respect RTL - true', async () => {
      const { component } = setup(AccordionContent)
      await component.$set({ expanded: true, rtl: true, key: '12345' });

      expect(screen.getByTestId('accordion-content')).toHaveClass('accordion-content-rtl');
    });
    it('Should respect RTL - false', async () => {
      const { component } = setup(AccordionContent)
      await component.$set({ expanded: true, rtl: false, key: '12345' });

      expect(screen.getByTestId('accordion-content')).not.toHaveClass('accordion-content-rtl');
    });
  })

  describe('Disabled', () => {
    it('Should respect disabled - true', async () => {
      const { component } = setup(AccordionContent)
      await component.$set({ expanded: true, disabled: true, key: '12345' });

      expect(screen.queryByTestId('accordion-content')).not.toBeInTheDocument();
    });
    it('Should respect disabled - false', async () => {
      const { component } = setup(AccordionContent)
      await component.$set({ expanded: true, disabled: false, key: '12345' });

      expect(screen.getByTestId('accordion-content')).not.toHaveClass('accordion-content--disabled');
    });
  });

  describe('Size', () => {
    it('Should respect small size prop', async () => {
      const { component } = setup(AccordionContent, { expanded: true, size: 'small' })
      await component.$set({ expanded: true, size: 'small', key: '12345' });
      expect(screen.getByTestId('accordion-content')).toHaveClass('accordion-content-size--small');
    });

    it('Should respect medium size prop', async () => {
      const { component } = setup(AccordionContent, { expanded: true, size: 'medium' })
      await component.$set({ expanded: true, size: 'medium', key: '12345' });

      expect(screen.getByTestId('accordion-content')).toHaveClass('accordion-content-size--medium');
    });

    it('Should respect large size prop', async () => {
      const { component } = setup(AccordionContent, { expanded: true, size: 'large' })
      await component.$set({ expanded: true, size: 'large', key: '12345' });

      expect(screen.getByTestId('accordion-content')).toHaveClass('accordion-content-size--large');
    });

    it('Should respect x-small size prop', async () => {
      const { component } = setup(AccordionContent, { expanded: true, size: 'x-small' })
      await component.$set({ expanded: true, size: 'x-small', key: '12345' });

      expect(screen.getByTestId('accordion-content')).toHaveClass('accordion-content-size--x-small');
    });
  })
});
