import { cleanup, render, screen } from '@testing-library/svelte';
import AccordionItem from '../src/AccordionItem.svelte';
import AccordionItemSlot from './AccordionItemSlot.mock.svelte';
import { describe, it, afterEach, expect } from 'vitest';
import '@testing-library/jest-dom';

const setup = (Component, props = {}) => {
  return {
    ...render(Component, props),
  };
};
const getAccordionItem = () => screen.getByTestId('Accordion-item');

describe('<AccordionItem />', () => {
  afterEach(cleanup);

  it('Should not explode without props', async () => {
    setup(AccordionItem);

    expect(getAccordionItem()).toBeInTheDocument();
  });

  it('Should respect slots', async () => {
    setup(AccordionItemSlot, {
      props: { root: { expanded: false }, content: 'Text content', label: 'Label title' },
    });

    expect(screen.getByTestId('accordion-button')).toBeInTheDocument();
    expect(screen.queryByTestId('accordion-content')).not.toBeInTheDocument();
  });

  it('Should respect expanded prop', async () => {
    setup(AccordionItemSlot, {
      props: { root: { expanded: true }, content: 'Text content', label: 'Label title' },
    });

    expect(screen.getByTestId('accordion-button')).toBeInTheDocument();
    expect(screen.queryByTestId('accordion-content')).toBeInTheDocument();
  });

  it('Should respect disabled prop', async () => {
    setup(AccordionItemSlot, {
      props: {
        root: { expanded: true, disabled: true },
        content: 'Text content',
        label: 'Label title',
      },
    });

    expect(screen.getByTestId('accordion-button')).toBeDisabled();
    expect(screen.queryByTestId('accordion-content')).not.toBeInTheDocument();
  });

  it('Should respect aria-controls ', async () => {
    setup(AccordionItemSlot, {
      props: { root: { expanded: true, id: '12345' }, label: 'Text label content' },
    });

    expect(screen.getByTestId('accordion-button')).toHaveAttribute(
      'aria-controls',
      'accordion-12345',
    );
  });

  it('Should respect aria-labbeledby', async () => {
    setup(AccordionItemSlot, {
      props: { root: { expanded: true, id: '12345' }, label: 'Text label content' },
    });

    expect(screen.getByTestId('accordion-content')).toHaveAttribute(
      'aria-labelledby',
      'accordion-control-12345',
    );
  });

  describe('RTL', () => {
    it('Should respect RTL - true', async () => {
      const { component } = setup(AccordionItem, { label: 'Text label content' });
      await component.$set({ rtl: true, expanded: true });

      expect(screen.getByTestId('accordion-button')).toHaveClass('accordion-title-rtl');
      expect(screen.getByTestId('accordion-content')).toHaveClass('accordion-content-rtl');
    });

    it('Should respect RTL - false', async () => {
      const { component } = setup(AccordionItem, { label: 'Text label content' });

      await component.$set({ rtl: false, expanded: true });

      expect(screen.getByTestId('accordion-button')).not.toHaveClass('accordion-title-rtl');
      expect(screen.getByTestId('accordion-content')).not.toHaveClass('accordion-content-rtl');
    });
  });

  describe('Skeleton', () => {
    it('Should render skeleton when loading', async () => {
      const { component } = setup(AccordionItem);
      await component.$set({ skeleton: true, expanded: true });

      expect(screen.getByTestId('Accordion-item-skeleton')).toBeInTheDocument();
    });

    it('Should not render skeleton when not loading', async () => {
      const { component } = setup(AccordionItem);
      await component.$set({ skeleton: false, expanded: true });

      expect(screen.queryByTestId('Accordion-item-skeleton')).not.toBeInTheDocument();
    });
  });

  describe('Size', () => {
    it('Should respect size - x-small', async () => {
      const { component } = setup(AccordionItem);
      await component.$set({ size: 'x-small', expanded: true });

      expect(screen.getByTestId('accordion-button')).toHaveClass('accordion-title-size--x-small');
      expect(screen.getByTestId('accordion-content')).toHaveClass(
        'accordion-content-size--x-small',
      );
    });
    it('Should respect size - small', async () => {
      const { component } = setup(AccordionItem);
      await component.$set({ size: 'small', expanded: true });

      expect(screen.getByTestId('accordion-button')).toHaveClass('accordion-title-size--small');
      expect(screen.getByTestId('accordion-content')).toHaveClass('accordion-content-size--small');
    });

    it('Should respect size - medium', async () => {
      const { component } = setup(AccordionItem);
      await component.$set({ size: 'medium', expanded: true });

      expect(screen.getByTestId('accordion-button')).toHaveClass('accordion-title-size--medium');
      expect(screen.getByTestId('accordion-content')).toHaveClass('accordion-content-size--medium');
    });

    it('Should respect size - large', async () => {
      const { component } = setup(AccordionItem);
      await component.$set({ size: 'large', expanded: true });

      expect(screen.getByTestId('accordion-button')).toHaveClass('accordion-title-size--large');
      expect(screen.getByTestId('accordion-content')).toHaveClass('accordion-content-size--large');
    });
  });

  describe('Variant', () => {
    it('Should respect variant - border', async () => {
      const { component } = setup(AccordionItem);
      await component.$set({ variant: 'border', expanded: true });

      expect(getAccordionItem()).not.toHaveClass('accordion-variant--fill');
    });
    it('Should respect variant - fill', async () => {
      const { component } = setup(AccordionItem);
      await component.$set({ variant: 'fill', expanded: true });

      expect(getAccordionItem()).toHaveClass('accordion-variant--fill');
    });
  });

  describe('Disabled', () => {
    it('Should respect disabled - true', async () => {
      const { component } = setup(AccordionItem);
      await component.$set({ disabled: true, expanded: true });

      expect(getAccordionItem()).toHaveClass('accordion-item-disabled');
      expect(screen.getByTestId('accordion-button')).toBeDisabled();
    });
    it('Should respect disabled - false', async () => {
      const { component } = setup(AccordionItem);
      await component.$set({ disabled: false, expanded: true });

      expect(getAccordionItem()).not.toHaveClass('accordion-item-disabled');
      expect(screen.getByTestId('accordion-button')).not.toBeDisabled();
    });
  });
});
