import { cleanup, render, screen } from '@testing-library/svelte';
import AccordionTitle from '../src/AccordionTitle.svelte';
import AccordionItemSlot from './AccordionItemSlot.mock.svelte';
import userEvent from '@testing-library/user-event';
import { describe, it, afterEach, expect, vi } from 'vitest';
import '@testing-library/jest-dom';

const setup = (Component, props = {}) => {
	return {
		user: userEvent.setup(),
		...render(Component, props)
	};
};
const getAccordionItem = () => screen.getByTestId('accordion-button');

describe('<AccordionTitle />', () => {
	afterEach(cleanup);

	it('Should not explode without props', async () => {
		setup(AccordionTitle);

		expect(getAccordionItem()).toBeInTheDocument();
	});

	it('Should respect label prop', async () => {
		const { component } = setup(AccordionTitle);

		await component.$set({ label: 'Text label content' });

		expect(screen.getByText('Text label content')).toBeInTheDocument();
	});

	it('Should respect slot rather than label prop', async () => {
		setup(AccordionItemSlot, {
			props: { root: { expanded: true }, titleSlot: 'Slot content', label: 'Text label content' }
		});

		expect(screen.getByText('Slot content')).toBeInTheDocument();
	});

	it('Should respect disabled prop', async () => {
		const { component } = setup(AccordionTitle);

		await component.$set({ disabled: true });

		expect(getAccordionItem()).toBeDisabled();
	});

	it('Should respect expanded prop', async () => {
		const { component } = setup(AccordionTitle);

		await component.$set({ expanded: true });

		expect(getAccordionItem()).toHaveAttribute('aria-expanded', 'true');
	});

	it('Should respect aria-controls', async () => {
		const { component } = setup(AccordionTitle);

		await component.$set({ key: '12345' });

		expect(getAccordionItem()).toHaveAttribute('aria-controls', 'accordion-12345');
	});

	describe('RTL', () => {
		it('Should respect RTL - true', async () => {
			const { component } = setup(AccordionTitle);
			await component.$set({ expanded: true, rtl: true, key: '12345' });

			expect(getAccordionItem()).toHaveClass('accordion-title-rtl');
		});

		it('Should respect RTL - false', async () => {
			const { component } = setup(AccordionTitle);
			await component.$set({ expanded: true, rtl: false, key: '12345' });

			expect(getAccordionItem()).not.toHaveClass('accordion-title-rtl');
		});
	});

	describe('Skeleton', () => {
		it('Should render skeleton when loading', async () => {
			const { component } = setup(AccordionTitle);
			await component.$set({ skeleton: true });

			expect(screen.getByTestId('Accordion-title-skeleton')).toBeInTheDocument();
		});

		it('Should not render skeleton when not loading', async () => {
			const { component } = setup(AccordionTitle);
			await component.$set({ skeleton: false });

			expect(screen.queryByTestId('Accordion-title-skeleton')).not.toBeInTheDocument();
		});
	});

	describe('Size', () => {
		it('Should respect x-small size prop', async () => {
			const { component } = setup(AccordionTitle, { size: 'small' });
			await component.$set({ size: 'x-small' });
			expect(getAccordionItem()).toHaveClass('accordion-title-size--x-small');
		});

		it('Should respect small size prop', async () => {
			const { component } = setup(AccordionTitle, { size: 'small' });
			await component.$set({ size: 'small' });
			expect(getAccordionItem()).toHaveClass('accordion-title-size--small');
		});

		it('Should respect medium size prop', async () => {
			const { component } = setup(AccordionTitle, { size: 'medium' });
			await component.$set({ size: 'medium' });
			expect(getAccordionItem()).toHaveClass('accordion-title-size--medium');
		});

		it('Should respect large size prop', async () => {
			const { component } = setup(AccordionTitle, { size: 'large' });
			await component.$set({ size: 'large' });
			expect(getAccordionItem()).toHaveClass('accordion-title-size--large');
		});
	});

	describe('Events', () => {
		it('Should fire click event', async () => {
			const onClick = vi.fn().mockImplementation(() => {});
			const { user, component } = setup(AccordionTitle);
			component.$on('click', onClick);
			const option = getAccordionItem();

			await user.click(option);

			expect(onClick).toHaveBeenCalled();
			expect(onClick).toHaveBeenCalledTimes(1);
		});

		it(`Should respect on:mouseenter event`, async () => {
			const mockEventHandler = vi.fn().mockImplementation(() => {});
			const { user, component } = setup(AccordionTitle);
			component.$on('mouseenter', mockEventHandler);
			const option = getAccordionItem();
			await user.pointer({ target: option });

			expect(mockEventHandler).toHaveBeenCalled();
			expect(mockEventHandler).toHaveBeenCalledTimes(1);
		});
	});
});
