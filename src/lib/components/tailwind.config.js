import { Config } from '@vrn-design/tailwind-config';

const config = {
  presets: [Config],
  content: [
    '../../../node_modules/@vrn-design/**/**/*.{js,svelte,ts}',
    './node_modules/@vrn-design/**/**/*.{js,svelte,ts}',
    // '../../node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'
  ],
};

export default config;