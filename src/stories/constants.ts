const FIGMA_URL = 'https://www.figma.com/file/d3EELtl4dLRIIyGruQ7Sk8/Glance-Design-System-%7C-Components?type=design&t=P0yosSjl8BnnTZ1y-1';
const FOUNDATION_URL = 'https://www.figma.com/file/fevkujruHQSVuv8m9cXATK/Glance-Design-System-%7C-Foundation?type=design';
/**
 * Figma links to components design
 * @constant
 */
export const FIGMA_DESIGN_SOURCES = {
  BUTTON: `${FIGMA_URL}&node-id=2618%3A9094`,
  ACCORDION: `${FIGMA_URL}&node-id=3267%3A12527`,
  CHECKBOX: `${FIGMA_URL}&node-id=2864%3A8200`,
  RADIO: `${FIGMA_URL}&node-id=2023%3A10`,
  ICON: `${FIGMA_URL}&node-id=2464%3A7962`,
  NOTIFICATION: `${FIGMA_URL}&node-id=2316%3A11646`,
  PRODUCT: {
    BUTTON: `${FIGMA_URL}&node-id=2054%3A3886`,
  },
  TABBAR: `${FIGMA_URL}&node-id=1%3A670`,
  TOGGLE: `${FIGMA_URL}&node-id=1821%3A4742`,
  TEXT_FIELD: `${FIGMA_URL}&node-id=4071%3A16964`,
  LOGO: `${FIGMA_URL}&node-id=1201%3A9101`,
  GLANCE_LOADER: `${FIGMA_URL}&node-id=2464%3A7196`,
  TOOLTIP: `${FIGMA_URL}&node-id=7347%3A49`,
  TYPOGRAPHY: `${FIGMA_URL}&node-id=7423%3A2048`,
  LINK: `${FIGMA_URL}&node-id=7545%3A89187`,
  SPINNER: `${FIGMA_URL}&node-id=9733%3A23088`,

  // theme
  THEME: `${FOUNDATION_URL}&node-id=588%3A169`,
};
