import {Config} from './src/lib/packages/tailwind-config/src/theme.js';

const config = {
  presets: [Config],
  content: [
    './src/stories/**/*.svelte',
    './src/lib/packages/spinner/src/**/*.{svelte,js,ts}',
    './src/lib/packages/**/**/*.svelte',
    './src/lib/packages/**/*.svelte', // for using 'packages/ui'
  ],
};

export default config;